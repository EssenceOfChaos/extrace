defmodule Extrace do
  @moduledoc """
  Documentation for Extrace.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Extrace.hello()
      :world

  """
  def hello do
    :world
  end
end
